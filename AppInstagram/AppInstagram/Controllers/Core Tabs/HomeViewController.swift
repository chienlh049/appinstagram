//
//  ViewController.swift
//  AppInstagram
//
//  Created by Chiến Lê on 23/10/2022.
//
import Firebase
import UIKit

class HomeViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        handletNotAuthenticated()
        
        do {
           try Auth.auth().signOut()
        }
        catch {
            print("failed to sign out")
        }
    }

    private func handletNotAuthenticated() {
        //check auth status
        if Auth.auth().currentUser == nil {
            // show log in
            let loginVC = LogInViewController()
            loginVC.modalPresentationStyle = .fullScreen
            present(loginVC, animated: false)
        }
    }
}

